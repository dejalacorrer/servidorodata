package myservice.mynamespace.service;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.http.HttpMethod;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;


public class Almacen {
    
    
    public static final String CajasEntidad = "Cajas";
    public static final String CajasId = "id";
    public static final String CajasNombre = "Nombre";
    public static final String CajasCaracteristicas = "Caracteristicas";
 

	private List<Entity> listaDeCajas;
        
        private static int SecuenciaId = 0;


	public Almacen() {
		listaDeCajas = new ArrayList<Entity>();
		initSampleData();
	}

	/* PUBLIC FACADE */

	public EntityCollection readEntitySetData(EdmEntitySet edmEntitySet)throws ODataApplicationException{

		// actually, this is only required if we have more than one Entity Sets
		if(edmEntitySet.getName().equals(DemoEdmProvider.ES_PRODUCTS_NAME)){
			return getCajas();
		}

		return null;
	}

	public Entity readEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams) throws ODataApplicationException{

		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		// actually, this is only required if we have more than one Entity Type
		if(edmEntityType.getName().equals(DemoEdmProvider.ET_PRODUCT_NAME)){
			return getCaja(edmEntityType, keyParams);
		}

		return null;
	}

        
        public Entity createEntityData(EdmEntitySet edmEntitySet, Entity entityToCreate) {

          EdmEntityType edmEntityType = edmEntitySet.getEntityType();

          // actually, this is only required if we have more than one Entity Type
          if (edmEntityType.getName().equals(DemoEdmProvider.ET_PRODUCT_NAME)) {
            return crearCaja(edmEntityType, entityToCreate);
          }

          return null;
        }

        /**
         * This method is invoked for PATCH or PUT requests
         * */
        public void updateEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams, Entity updateEntity,
            HttpMethod httpMethod) throws ODataApplicationException {

          EdmEntityType edmEntityType = edmEntitySet.getEntityType();

          // actually, this is only required if we have more than one Entity Type
          if (edmEntityType.getName().equals(DemoEdmProvider.ET_PRODUCT_NAME)) {
            updateCaja(edmEntityType, keyParams, updateEntity, httpMethod);
          }
        }

        public void deleteEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
            throws ODataApplicationException {

          EdmEntityType edmEntityType = edmEntitySet.getEntityType();

          // actually, this is only required if we have more than one Entity Type
          if (edmEntityType.getName().equals(DemoEdmProvider.ET_PRODUCT_NAME)) {
            deleteCaja(edmEntityType, keyParams);
          }
        }
        


	/*  INTERNAL */

	private EntityCollection getCajas(){
		EntityCollection retEntitySet = new EntityCollection();

		for(Entity productEntity : this.listaDeCajas){
			   retEntitySet.getEntities().add(productEntity);
		}

		return retEntitySet;
	}


	private Entity getCaja(EdmEntityType edmEntityType, List<UriParameter> keyParams) throws ODataApplicationException{

		// the list of entities at runtime
		EntityCollection entitySet = getCajas();
		
		/*  generic approach  to find the requested entity */
		Entity requestedEntity =  null; // Util.findEntity(edmEntityType, entitySet, keyParams);
                
                if (( keyParams.size()==1 ) && ( CajasId.equals(keyParams.get(0).getName()))) 
                {
                    int idn = Integer.parseInt( keyParams.get(0).getText() );
                    for (Iterator iterator = entitySet.iterator(); iterator.hasNext();) {
                        Entity next = (Entity) iterator.next();
                        Object valueObject = next.getProperty( CajasId ).getValue();
                        if ( valueObject.equals( idn ) )
                        {
                            return next;
                        }
                    }
                }
		
		if(requestedEntity == null){
			// this variable is null if our data doesn't contain an entity for the requested key
			// Throw suitable exception
			throw new ODataApplicationException("Entity for requested key doesn't exist",
                                    HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}

	/* HELPER */

	private void initSampleData(){

		// add some sample product entities
              final Entity e1 = new Entity()
                      .addProperty(new Property(null, CajasId, ValueType.PRIMITIVE,  this.ProximoId() ))
                .addProperty(new Property(null, CajasNombre, ValueType.PRIMITIVE, "Caja Verde"))
                .addProperty(new Property(null, CajasCaracteristicas, ValueType.PRIMITIVE,
                        "Tiene color verde"));
                e1.setId(createId( CajasEntidad, 1));
		listaDeCajas.add(e1);

                final Entity e2 = new Entity()
                    .addProperty(new Property(null, CajasId, ValueType.PRIMITIVE, this.ProximoId()))
                    .addProperty(new Property(null, CajasNombre, ValueType.PRIMITIVE, "Caja Azul"))
                    .addProperty(new Property(null, CajasCaracteristicas, ValueType.PRIMITIVE,
                        "Reforzada"));
                e2.setId(createId( CajasEntidad, 2));
		listaDeCajas.add(e2);

                final Entity e3 = new Entity()
                    .addProperty(new Property(null, CajasId, ValueType.PRIMITIVE, this.ProximoId()))
                    .addProperty(new Property(null, CajasNombre, ValueType.PRIMITIVE, "Caja Roja"))
                    .addProperty(new Property(null, CajasCaracteristicas, ValueType.PRIMITIVE,
                        "Personal"));
                e3.setId(createId(CajasEntidad, 3));
		listaDeCajas.add(e3);
	}

	private URI createId(String entitySetName, Object id) {
		try {
			return new URI(entitySetName + "(" + String.valueOf(id) + ")");
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create id for entity: " + entitySetName, e);
		}
	}
        
        
  private Entity crearCaja(EdmEntityType edmEntityType, Entity entity) {

    // the ID of the newly created product entity is generated automatically
    int newId = this.ProximoId();

    Property idProperty = entity.getProperty(CajasId);
    if (idProperty != null) {
      idProperty.setValue(ValueType.PRIMITIVE, newId );
    } else {
      // as of OData v4 spec, the key property can be omitted from the POST request body
      entity.getProperties().add(new Property(null, CajasId, ValueType.PRIMITIVE, newId));
    }
    entity.setId(createId( CajasEntidad, newId));
    this.listaDeCajas.add(entity);

    return entity;

  }

  private boolean productIdExists(int id) {

    for (Entity entity : this.listaDeCajas) {
      Integer existingID = (Integer) entity.getProperty(CajasId).getValue();
      if (existingID.intValue() == id) {
        return true;
      }
    }

    return false;
  }

  private void updateCaja(EdmEntityType edmEntityType, List<UriParameter> keyParams, Entity entity,
      HttpMethod httpMethod) throws ODataApplicationException {

    Entity productEntity = getCaja(edmEntityType, keyParams);
    if (productEntity == null) {
      throw new ODataApplicationException("La Caja no se encuentra", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
    }

    // loop over all properties and replace the values with the values of the given payload
    // Note: ignoring ComplexType, as we don't have it in our odata model
    List<Property> existingProperties = productEntity.getProperties();
    for (Property existingProp : existingProperties) {
      String propName = existingProp.getName();

      
      // ignore the key properties, they aren't updateable
      if ( CajasId.equals(propName))  {
        continue;
      }

      Property updateProperty = entity.getProperty(propName);
      // the request payload might not consider ALL properties, so it can be null
      if (updateProperty == null) {
        // if a property has NOT been added to the request payload
        // depending on the HttpMethod, our behavior is different
        if (httpMethod.equals(HttpMethod.PATCH)) {
          // as of the OData spec, in case of PATCH, the existing property is not touched
          continue; // do nothing
        } else if (httpMethod.equals(HttpMethod.PUT)) {
          // as of the OData spec, in case of PUT, the existing property is set to null (or to default value)
          existingProp.setValue(existingProp.getValueType(), null);
          continue;
        }
      }

      // change the value of the properties
      existingProp.setValue(existingProp.getValueType(), updateProperty.getValue());
    }
  }

  private void deleteCaja(EdmEntityType edmEntityType, List<UriParameter> keyParams)
      throws ODataApplicationException {

    Entity productEntity = getCaja(edmEntityType, keyParams);
    if (productEntity == null) {
      throw new ODataApplicationException("Entity not found", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
    }

    this.listaDeCajas.remove(productEntity);
  }
  
  
  private int ProximoId() {
      Almacen.SecuenciaId ++;
      
      return Almacen.SecuenciaId;
              
  }
        
}

